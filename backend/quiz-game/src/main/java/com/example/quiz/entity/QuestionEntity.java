package com.example.quiz.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "question")
public class QuestionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "question_name")
    private String questionName;

    @Column(name = "question_text")
    private String questionText;

    @Column(name = "question_level")
    private int questionLevel;

    @OneToOne
    @JoinColumn(name = "options")
    private OptionEntity optionEntity;

    @Column(name = "topic_no")
    private int topicNo;

}

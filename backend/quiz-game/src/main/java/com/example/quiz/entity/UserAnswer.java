package com.example.quiz.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_answer")
public class UserAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "total_ques")
    private int totalQues;

    @Column(name = "correct_ans")
    private int correctAnswer;
}

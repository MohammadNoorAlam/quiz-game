package com.example.quiz.repository;

import com.example.quiz.entity.OptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface OptionRepository extends JpaRepository<OptionEntity, Long> {
}

package com.example.quiz.repository;

import com.example.quiz.entity.UserAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAnsRepository extends JpaRepository<UserAnswer, Long> {
    UserAnswer findByName(String name);
}

package com.example.quiz.core;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.json.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.List;

public class CoreFuntions {

    public static String getString(JSONObject json, String key) {
        if (json.has(key)) {
            if (json.NULL != json.get(key) && !StringUtils.isBlank(json.get(key).toString()) && !json.get(key).equals("{}")) {
                return json.get(key).toString();
            } else {
                return null;
            }
        }
        return null;
    }

    public static <T> T objectMapperReadValue(String content, Class<T> valueType) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(content,
                    valueType);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> List<T> objectMapperReadArrayValue(String mapperArrStr, Class<T> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return (List<T>) objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .readValue(mapperArrStr, TypeFactory.defaultInstance().constructCollectionType(List.class, clazz));
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

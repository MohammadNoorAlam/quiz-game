package com.example.quiz.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "answer")
public class OptionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "question_no")
    private Long questionNo;

    @Column(name = "answer_no_1")
    private String answerNo1;

    @Column(name = "answer_no_2")
    private String answerNo2;

    @Column(name = "answer_no_3")
    private String answerNo3;

    @Column(name = "answer_no_4")
    private String answerNo4;

    @Column(name = "correct_answer")
    private int correctAnswer;

}

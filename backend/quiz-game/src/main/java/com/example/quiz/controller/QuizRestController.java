package com.example.quiz.controller;

import com.example.quiz.entity.QuestionEntity;
import com.example.quiz.entity.TopicEntity;
import com.example.quiz.entity.UserAnswer;
import com.example.quiz.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:4200")
public class QuizRestController {

    @Autowired
    private QuizService quizService;

    @GetMapping("/list")
    public List<QuestionEntity> getAllQuestion() {
        return quizService.list();
    }

    @GetMapping("/topic-list")
    public List<TopicEntity> getAllTopic() {
        return quizService.getTopicList();
    }

    @GetMapping("/topic")
    public Optional<TopicEntity> getTopic(@RequestParam Long topicId) {
        return quizService.findTopicById(topicId);
    }

    @PostMapping("/save-question")
    public ResponseEntity<?> saveQuestion(@RequestBody QuestionEntity theQuesAns) {
        quizService.saveQuesAns(theQuesAns);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/exam-filter")
    public ResponseEntity<List<QuestionEntity>> getQuestionList(@RequestParam int count, @RequestParam int topicId, @RequestParam int quesLevel) {
        List<QuestionEntity> quesList = new ArrayList<QuestionEntity>();
        List<QuestionEntity> totalQuesList = quizService.findQuesByCustomData(topicId, quesLevel);
        if (null != totalQuesList && totalQuesList.size() > 0) {
            if (count >= totalQuesList.size()) {
                quesList.addAll(totalQuesList);
            } else if (count < totalQuesList.size()) {

                for (int i = 0; i < count; i++) {
                    quesList.add(totalQuesList.get(i));
                }
            }
        }
        return new ResponseEntity<>(quesList, HttpStatus.OK);
    }

    @PostMapping("/save-result")
    public ResponseEntity<?> saveResult(@RequestBody UserAnswer theUserAnswer) {
        quizService.saveResult(theUserAnswer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/view-result")
    public ResponseEntity<?> viewResult(@RequestParam String name) {
        UserAnswer theUserAns = quizService.viewResult(name);
        return new ResponseEntity<>(theUserAns, HttpStatus.OK);
    }

}

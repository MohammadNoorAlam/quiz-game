package com.example.quiz.service;

import com.example.quiz.entity.QuestionEntity;
import com.example.quiz.entity.TopicEntity;
import com.example.quiz.entity.UserAnswer;

import java.util.List;
import java.util.Optional;

public interface QuizService {
    List<QuestionEntity> list();
    void saveQuesAns(QuestionEntity theQuesAns);
    List<QuestionEntity> findQuesByCustomData(int topicId, int quesLevel);
    List<TopicEntity> getTopicList();
    Optional<TopicEntity> findTopicById(Long topicId);
    void saveResult(UserAnswer theUserAnswer);
    UserAnswer viewResult(String name);
}

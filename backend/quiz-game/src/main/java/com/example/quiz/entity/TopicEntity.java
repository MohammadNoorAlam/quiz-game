package com.example.quiz.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "topic")
public class TopicEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "topic_name")
    private String topicName;

}

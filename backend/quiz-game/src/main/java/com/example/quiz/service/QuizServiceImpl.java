package com.example.quiz.service;

import com.example.quiz.entity.OptionEntity;
import com.example.quiz.entity.QuestionEntity;
import com.example.quiz.entity.TopicEntity;
import com.example.quiz.entity.UserAnswer;
import com.example.quiz.repository.OptionRepository;
import com.example.quiz.repository.QuestionRepository;
import com.example.quiz.repository.TopicRepository;
import com.example.quiz.repository.UserAnsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private OptionRepository optionRepository;
    @Autowired
    private TopicRepository topicRepository;
    @Autowired
    private UserAnsRepository userAnsRepository;

    @Override
    public List<QuestionEntity> list() {
        return questionRepository.findAll();
    }

    @Override
    @Transactional
    public void saveQuesAns(QuestionEntity theQuesAns) {
        if (theQuesAns != null) {
            OptionEntity theOption = new OptionEntity();
            theOption.setQuestionNo(theQuesAns.getId());
            optionRepository.save(theQuesAns.getOptionEntity());
            questionRepository.save(theQuesAns);
        }
//        QuestionEntity questionObj = null;
//        JSONObject json = new JSONObject(theQuesAns);
//        String questionObjStr = CoreFuntions.getString(json, "questionObj");
//        if (questionObjStr != null && !questionObjStr.equals("{}")) {
//            questionObj = CoreFuntions.objectMapperReadValue(questionObjStr, QuestionEntity.class);
//        }
//        if (questionObj != null) {
//            QuestionEntity theQues = questionRepository.save(questionObj);
//            // answer save
//            String answerObjStr = CoreFuntions.getString(json, "answerObj");
//            if (answerObjStr != null && !answerObjStr.equals("{}")) {
//                OptionEntity answerObj = null;
//                answerObj = CoreFuntions.objectMapperReadValue(answerObjStr, OptionEntity.class);
//                answerObj.setQuestionNo(theQues.getId());
//                if (answerObj != null) {
//                    optionRepository.save(answerObj);
//                }
//            }
//        }
    }

    @Override
    public List<QuestionEntity> findQuesByCustomData(int topicId, int quesLevel) {
        return questionRepository.findByTopicNoAndQuestionLevel(topicId, quesLevel);
    }

    @Override
    public List<TopicEntity> getTopicList() {
        return topicRepository.findAll();
    }

    @Override
    public Optional<TopicEntity> findTopicById(Long topicId) {
        return topicRepository.findById(topicId);
    }

    @Override
    @Transactional
    public void saveResult(UserAnswer theUserAnswer) {
        userAnsRepository.save(theUserAnswer);
    }

    @Override
    public UserAnswer viewResult(String name) {
        return userAnsRepository.findByName(name);
    }


}

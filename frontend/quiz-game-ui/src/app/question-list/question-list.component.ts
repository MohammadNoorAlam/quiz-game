import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Question } from '../coreQuizGame/model/question';
import { Topic } from '../coreQuizGame/model/topic';
import { User } from '../coreQuizGame/model/user';
import { QuizService } from '../coreQuizGame/services/quiz.service';

@Component({
    selector: 'app-question-list',
    templateUrl: './question-list.component.html',
    styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {

    quesList: Question;
    quesFilter: any;
    topic: Topic;
    cAns: number;
    correct: any = 0;
    user: User;
    questionListFormGroup: FormGroup;
    resultFormGroup: FormGroup;

    constructor(private quizService: QuizService,
        private formBuilder: FormBuilder, 
        private router: Router) { }

    ngOnInit(): void {
        this.quesList = this.quizService.quesList;
        this.quesFilter = this.quizService.quesFilter;
        this.getTopicById();
        console.log(this.quesList);
        
        this.questionListFormGroup = this.formBuilder.group({
            saveAnsObj: this.formBuilder.group({
                name: this.quesFilter.name,
                questionNo: new FormControl(''),
                selectAnswer: new FormControl('')
            })
        });
    }

    getTopicById() {
        this.quizService.getTopicsById(this.quesFilter.topicId).subscribe(data => {
            this.topic = data;
            console.log(this.topic.topicName);
        });
    }

    getAns(id: number) {
        this.cAns = id;
    }

    submit() {
        let list = this.questionListFormGroup.controls['saveAnsObj'].value;
        if (list.selectAnswer != this.cAns) {
            console.log("dont match");
            return;
        }
        console.log("match");
        this.correct++;
        
    }

    saveResult() {
        this.user = {
            name: this.quesFilter.name,
            totalQues: Object.keys(this.quesList).length,
            correctAnswer: this.correct,
        };
        console.log(this.user);
        this.quizService.saveResult(this.user).subscribe(data => {
            this.router.navigate(['/quiz/submission']);
        });
    }


}

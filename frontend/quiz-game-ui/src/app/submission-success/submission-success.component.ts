import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from '../coreQuizGame/services/quiz.service';

@Component({
  selector: 'app-submission-success',
  templateUrl: './submission-success.component.html',
  styleUrls: ['./submission-success.component.css']
})
export class SubmissionSuccessComponent implements OnInit {

  quesFilter: any;  

  constructor(private quizService: QuizService, 
              private router: Router) { }

  ngOnInit(): void {
    this.quesFilter = this.quizService.quesFilter;
  }

  viewResult() {
    this.quizService.viewResult(this.quesFilter.name).subscribe(data => {
      this.quizService.resultItem = data;
      console.log(data);
      
      this.router.navigate(['/quiz/result']);
    });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResultComponent } from './result/result.component';
import { QuestionListComponent } from './question-list/question-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AddquestionComponent } from './addquestion/addquestion.component';
import { SubmissionSuccessComponent } from './submission-success/submission-success.component';

const routes: Routes = [
  { path: 'quiz/result', component: ResultComponent},
  { path: 'quiz/submission', component: SubmissionSuccessComponent},
  { path: 'quiz/question', component: QuestionListComponent},
  { path: 'quiz/add-question', component: AddquestionComponent},
  { path: 'start-quiz', component: DashboardComponent},
  { path: '', redirectTo: 'start-quiz', pathMatch: 'full' },
  { path: '**', redirectTo: 'start-quiz', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ResultComponent,
    QuestionListComponent,
    NavbarComponent,
    AddquestionComponent,
    SubmissionSuccessComponent, 
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule, 
    ReactiveFormsModule, 
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

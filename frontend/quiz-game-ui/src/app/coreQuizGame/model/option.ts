export class Option {
    answerNo1: string;
    answerNo2: string;
    answerNo3: string;
    answerNo4: string;
    questionNo: number;
    correctAnswer: number;
}

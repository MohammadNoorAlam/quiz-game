import { Option } from "./option";

export class Question {
    id: number;
    questionName: string;
    questionText: String;
    questionLevel: number;
    topicNo: number;
    optionEntity: Option;

    isHidded: boolean = false;
}

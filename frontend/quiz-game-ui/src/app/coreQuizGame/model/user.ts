export class User {
    name: string;
    totalQues: number;
    correctAnswer: number;
}

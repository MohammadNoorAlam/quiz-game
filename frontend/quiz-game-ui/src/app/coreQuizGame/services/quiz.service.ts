import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Question } from '../model/question';
import { User } from '../model/user';

@Injectable({
    providedIn: 'root'
})
export class QuizService {

    quesList: Question;
    resultItem: User;
    quesFilter: Subject<any> = new BehaviorSubject<any>(0);

    constructor(private http: HttpClient) { }

    saveQuestion(reqObj: Question): Observable<any> {
        return this.http.post(environment.baseUrl + environment.apiPath + "/save-question", reqObj);
    }

    getQues(count: number, topicId: number, quesLevel: number): Observable<any> {
        return this.http.get(environment.baseUrl + environment.apiPath + '/exam-filter?topicId=' + topicId + '&quesLevel=' + quesLevel + '&count=' + count)
    }

    getTopics(): Observable<any> {
        return this.http.get(environment.baseUrl + environment.apiPath + '/topic-list');
    }

    getTopicsById(id: number): Observable<any> {
        return this.http.get(environment.baseUrl + environment.apiPath + '/topic?topicId=' + id);
    }

    saveResult(reqObj: User): Observable<any> {
        return this.http.post(environment.baseUrl + environment.apiPath + '/save-result', reqObj);
    }

    viewResult(name: string): Observable<any> {
        return this.http.get(environment.baseUrl + environment.apiPath + '/view-result?name=' + name);
    }
}

import { Component, OnInit } from '@angular/core';
import { QuizService } from '../coreQuizGame/services/quiz.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  resultItems: any;

  constructor(private quizService: QuizService) { }

  ngOnInit(): void {
    this.resultItems = this.quizService.resultItem;
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Question } from '../coreQuizGame/model/question';
import { QuizService } from '../coreQuizGame/services/quiz.service';

@Component({
    selector: 'app-addquestion',
    templateUrl: './addquestion.component.html',
    styleUrls: ['./addquestion.component.css']
})
export class AddquestionComponent implements OnInit {

    successMessage: string;
    errorMessage: string;

    saveQuesAnsFormGroup: FormGroup;
    optionsFormGroup: FormGroup;

    question: Question;

    constructor(private quizService: QuizService,
        private formBuilder: FormBuilder) { }

    ngOnInit(): void {
        this.saveQuesAnsFormGroup = this.formBuilder.group({
            saveQuesAnsObj: this.formBuilder.group({
                questionName: new FormControl('', [Validators.required]),
                questionText: new FormControl(''),
                questionLevel: new FormControl('', [Validators.required]),
                topicNo: new FormControl('', [Validators.required]),
            }),
            optionObj: this.formBuilder.group({
                answerNo1: new FormControl('', [Validators.required]),
                answerNo2: new FormControl('', [Validators.required]),
                answerNo3: new FormControl('', [Validators.required]),
                answerNo4: new FormControl('', [Validators.required]),
                correctAnswer: new FormControl('', [Validators.required])
            })
        });
    }

    saveQuesAns() {
        this.question = this.saveQuesAnsFormGroup.controls['saveQuesAnsObj'].value;
        this.question.optionEntity = this.saveQuesAnsFormGroup.controls['optionObj'].value;
        
        this.quizService.saveQuestion(this.question).subscribe(data => {
            this.saveQuesAnsFormGroup.reset();
            this.successMessage = "Question Set successfull";
        }, err => {
            this.errorMessage = "All fields are required!"
        });
    }

}

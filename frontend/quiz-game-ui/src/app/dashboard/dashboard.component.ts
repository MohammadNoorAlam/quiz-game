import { Component, OnInit } from '@angular/core';

import { QuizService } from '../coreQuizGame/services/quiz.service';
import { Topic } from '../coreQuizGame/model/topic';
import { Question } from '../coreQuizGame/model/question';
import { Option } from '../coreQuizGame/model/option';
import { QuesFilter } from '../coreQuizGame/model/quesFilter';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { element } from "protractor";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    successMessage: string;
    errorMessage: string;

    topicList: Topic;
    quesList: Question[];
    startGameFormGroup: FormGroup;
    quesFilter: QuesFilter;
    optionList: Option[];

    constructor(private quizService: QuizService,
        private formBuilder: FormBuilder,
        private router: Router) { }

    ngOnInit(): void {
        this.startGameFormGroup = this.formBuilder.group({
            quizFilterObj: this.formBuilder.group({
                name: new FormControl('', [Validators.required]),
                count: new FormControl('', [Validators.required]),
                topicId: new FormControl('', [Validators.required]),
                quesLevel: new FormControl('', [Validators.required]),
            })
        });
        this.getTopicList();
    }

    getQuesList() {
        this.quesFilter = this.startGameFormGroup.controls['quizFilterObj'].value;

        this.quizService.viewResult(this.quesFilter.name).subscribe(data => {
            if (data) {
                this.errorMessage = "Name is already found! Please use an unique name.";
            }
            else {
                this.quizService.getQues(this.quesFilter.count, this.quesFilter.topicId, this.quesFilter.quesLevel).subscribe(data => {
                    this.quesList = data;
                    this.quizService.quesList = data;
                    this.quizService.quesFilter = this.startGameFormGroup.controls['quizFilterObj'].value;
                    this.router.navigate(['quiz/question']);
                }, err => {
                    this.errorMessage = "All fields are required!";
                });
            }
        });
    }

    getTopicList() {
        this.quizService.getTopics().subscribe(data => {
            this.topicList = data;
        });
    }

}
